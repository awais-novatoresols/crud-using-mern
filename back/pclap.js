var Author = require("./author");
const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  Author.find()
    .then(author => res.json(author))
    .catch(err => res.status(404).json({ eror: "No Author record fond" }));
});

router.get("/:id", (req, res) => {
  Author.findById(req.params.id)
    .then(author => res.json(author))
    .catch(err => res.status((404).json({ Eerr: "No book fond" })));
});

router.post("/", (req, res) => {
  Author.create(req.body)
    .then(author => res.json({ msg: "Author added successfully" }))
    .catch(err => res.status(400).json({ error: "unable to add author" }));
});

router.delete("/:id", (req, res) => {
  Author.findByIdAndRemove(req.params.id, req.body)
    .then(author => res.json({ msg: "Author deleted successfully" }))
    .catch(err => res.json(404).json({ err: "No Suchbook fond" }));
});

router.put("/:id", (req, res) => {
  Author.findByIdAndUpdate(req.params.id, req.body)
    .then(author => res.json({ msg: "Updated succcessfully" }))
    .catch(err => res.status(404).json({ error: "No such book fond" }));
});

module.exports = router;
