const mongoose = require("mongoose");

const bookSchema = mongoose.Schema({
  Name: {
    pk: String,
    isbn: Number
  }
});

var Book = mongoose.model("Book-and-Isbn", bookSchema);
module.exports = Book;
