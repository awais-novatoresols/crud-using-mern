var pclap = require("./pclap");
var book = require("./Booksk");
var mongoose = require("mongoose");
const express = require("express");
const app = express();
var cors = require("cors");

mongoose.connect(
  "mongodb+srv://user1:1234@cluster0-hxlcg.mongodb.net/test?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  function(err) {
    if (err) throw err;

    console.log("DBConnected successfully...");

    //     var author = new Author({
    //       title: {
    //         name: "Imam",
    //         age: 84
    //       }
    //     });
    //     author.save(function(err) {
    //       if (err) throw err;
    //       console.log("User added successfully");
    //     });
  }
);

app.use(cors({ origin: true, credentials: true }));

// Init Middleware
app.use(express.json({ extended: false }));

app.get("/", (req, res) => res.send("Hello world I am runnign on!"));
app.use("/api/authors", pclap);

app.use("/api/books", book);

app.listen(3300);

console.log("server running at port 3300");
