var Book = require("./book");
const express = require("express");
const router = express.Router();
router.post("/", (req, res) => {
  const newBook = new Book({
    Name: {
      pk: req.body.pk,
      isbn: req.body.isbn
    }
  });
  newBook
    .save()
    .then(book => res.json({ msg: "Book added successfully" }))
    .catch(err => res.status(404).json({ err: "unable to add book" }));
});

module.exports = router;
