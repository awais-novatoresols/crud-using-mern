const mongoose = require("mongoose");

const authorSchema = mongoose.Schema({
  name: {
    type: String
  },
  age: {
    type: Number
  }
});

var Author = mongoose.model("nameOfAuthors", authorSchema);
module.exports = Author;
