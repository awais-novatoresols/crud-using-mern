import React, { Component } from "react";
import axios from "axios";

class CreateBook extends Component {
  constructor() {
    super();
    this.state = {
      pk: "",
      isbn: ""
    };
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onSubmit = e => {
    e.preventDefault();
    const dat = {
      pk: this.state.pk,
      isbn: this.state.isbn
    };
    axios
      .post("http://localhost:3300/api/books", dat)
      .then(res => {
        console.log("api response", res);
        this.setState({
          pk: "",
          isbn: ""
        });
      })
      .catch(err => {
        console.log("error in adding book");
      });
  };

  render() {
    return (
      <div className="CreateBook">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Add Book</h1>
              <p className="lead text-center">Create new book</p>
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Name of Book"
                    name="pk"
                    value={this.state.pk}
                    onChange={this.onChange}
                  />
                </div>
                <br />
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="ISBN "
                    name="isbn"
                    value={this.state.isbn}
                    onChange={this.onChange}
                  />
                </div>
                <br />
                <input
                  type="submit"
                  className="btn btn-outline-warning "
                  onSubmit={this.onSubmit}
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateBook;
