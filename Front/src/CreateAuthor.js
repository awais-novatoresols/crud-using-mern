import React, { Component } from "react";
import axios from "axios";

class CreateAuthor extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      age: ""
    };
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onSubmit = e => {
    e.preventDefault();
    const data = {
      name: this.state.name,
      age: this.state.age
    };
    axios
      .post("http://localhost:3300/api/authors", data)
      .then(res => {
        console.log("api response", res);
        this.setState({
          name: "",
          age: ""
        });
        //this.props.history.push("/");
      })
      .catch(err => {
        console.log("Error in adding author");
      });
  };

  render() {
    return (
      <div className="CreateBook">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Add Author</h1>
              <p className="lead text-center">Create new Author</p>
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="Author Name"
                    name="name"
                    className="form-control"
                    value={this.state.name}
                    onChange={this.onChange}
                  />
                </div>
                <br />
                <div className="form-group">
                  <input
                    className="form-control"
                    type="text"
                    placeholder="Age"
                    name="age"
                    value={this.state.age}
                    onChange={this.onChange}
                  />
                </div>
                <br />
                <div className="form-group">
                  <input
                    type="submit"
                    className="btn btn-outline-warning "
                    onSubmit={this.onSubmit}
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateAuthor;
