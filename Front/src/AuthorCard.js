import React from "react";
import { Link } from "react-router-dom";
import "./App.css";

const AuthorCard = props => {
  const author = props.author;

  return (
    <div className="card-container">
      <div className="desc">
        <h2>
          <Link to={`/Show-Authors/${author._id}`}>{author.name}</Link>
          <br /> Age: {author.age}
        </h2>
      </div>
    </div>
  );
};
export default AuthorCard;
