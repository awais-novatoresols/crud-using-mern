import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./App.css";
import axios from "axios";

class ShowAuthorDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      author: {}
    };
  }

  componentDidMount() {
    axios
      .get("http://localhost:3300/api/authors/" + this.props.match.params.id)
      .then(res => {
        this.setState({
          author: res.data
        });
      })
      .catch(err => {
        console.log("Error from ShowAuthorDetails");
      });
  }

  onDeleteClick(id) {
    axios
      .delete("http://localhost:3300/api/authors/" + id)
      .then(res => {
        this.props.history.push("/abc");
      })
      .catch(err => {
        console.log("Error from showAuthordetails while delete");
      });
  }
  render() {
    const author = this.state.author;
    let authItem = (
      <div>
        <table className="table table-hover table-dark">
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Name</td>
              <td>{author.name}</td>
            </tr>

            <tr>
              <th scope="row">2</th>
              <td>Age</td>
              <td>{author.age}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
    return (
      <div className="ShowBookDetails">
        <div className="container">
          <div className="row">
            <div className="col-md-10 m-auto"></div>
            <br /> <br />
          </div>
          <div>{authItem}</div>
          <div className="row">
            <div className="col-md-6">
              <button
                type="button"
                className="btn btn-outline-danger btn-lg btn-block"
                onClick={this.onDeleteClick.bind(this, author._id)}
              >
                Delete Author
              </button>
            </div>

            <div className="col-md-6">
              <Link
                to={`/Edit-Authors/${author._id}`}
                className="btn btn-outline-info btn-lg btn-block"
              >
                Edit Author
              </Link>
              <br />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ShowAuthorDetails;
