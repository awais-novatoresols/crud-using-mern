import React, { Component } from "react";
import "./App.css";
import axios from "axios";
import AuthorCard from "./AuthorCard";

class ShowAuthorList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      author: []
    };
  }

  componentDidMount() {
    axios
      .get("http://localhost:3300/api/authors")
      .then(res => {
        this.setState({
          author: res.data
        });
      })
      .catch(err => {
        console.log("Error from show authorlist");
      });
  }

  render() {
    const author = this.state.author;
    console.log("Print Author:" + author);
    let authorList;
    if (!author) {
      authorList = "There is no author record";
    } else {
      authorList = author.map((author, k) => (
        <AuthorCard author={author} key={k} />
      ));
    }

    return (
      <div className="ShowBookList">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <br />
              <h2 className="display-4 text-center">Authors List</h2>
            </div>
            <div className="col-md-11">
              <div className="list">{authorList}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ShowAuthorList;
