import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import CreateBook from "./CreateBook";
import CreateAuthor from "./CreateAuthor";
import ShowAuthordetails from "./ShowAuthorDetails";
import ShowAuthorList from "./ShowAuthorList";
import UpdateAuthorInfo from "./UpdateAuthorInfo";
class Home extends Component {
  render() {
    return (
      <div className="Create">
        <Router>
          <Link to="/">
            <h1 className="App-header">Books and Author Management</h1>
          </Link>
          <br />
          <Link to="/Create-Author" className="btn btn-outline-warning ">
            + Add Author
          </Link>
          <Link to="/Add-Book" className="btn btn-outline-warning">
            + Add Book
          </Link>

          <Link to="/abc" className="btn btn-outline-warning ">
            Show Authors
          </Link>

          <Link from="/*" to="/"></Link>
          <div>
            <Route path="/abc" component={ShowAuthorList} />
            <Route path="/Create-Author" component={CreateAuthor}></Route>
            <Route path="/Add-Book" component={CreateBook}></Route>
            <Route path="/Show-Authors/:id" component={ShowAuthordetails} />
            <Route path="/Edit-Authors/:id" component={UpdateAuthorInfo} />
          </div>
        </Router>
      </div>
    );
  }
}

export default Home;
