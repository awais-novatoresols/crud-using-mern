import React, { Component } from "react";
import axios from "axios";
import "./App.css";

class UpdateAuthorInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      age: ""
    };
  }

  componentDidMount() {
    axios
      .get("http://localhost:3300/api/authors/" + this.props.match.params.id)
      .then(res => {
        this.setState({
          name: res.data.name,
          age: res.data.age
        });
      })
      .catch(err => {
        console.log("Error from UpdateAuthorInfo");
      });
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onSubmit = e => {
    e.preventDefault();
    const data = {
      name: this.state.name,
      age: this.state.age
    };

    axios
      .put(
        "http://localhost:3300/api/authors/" + this.props.match.params.id,
        data
      )
      .then(res => {
        this.props.history.push("/Show-Authors/" + this.props.match.params.id);
      })
      .catch(err => {
        console.log("Error in updating author");
      });
  };

  render() {
    return (
      <div className="UpdateBookInfo">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <br />
            </div>
          </div>

          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label htmlFor="author">Name</label>
              <input
                type="text"
                placeholder="Author Name"
                name="name"
                className="form-control"
                value={this.state.name}
                onChange={this.onChange}
              />
            </div>

            <div className="form-group">
              <label htmlFor="author">Age</label>
              <input
                type="text"
                placeholder="Age"
                name="age"
                className="form-control"
                value={this.state.age}
                onChange={this.onChange}
              />
            </div>

            <button
              type="submit"
              className="btn btn-outline-info btn-lg btn-block"
              onSubmit={this.onSubmit}
            >
              Update Author
            </button>
          </form>
        </div>
      </div>
    );
  }
}
export default UpdateAuthorInfo;
